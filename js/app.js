//Decided to learn and implement jQuery just so see how different it is from react and why ppl prefer react
import taskList from './classes/taskList.js';
import task from './classes/task.js';

(function(){
  const app = {
    key: 'firstList',
    render(template) {
      $('main').append(template);
      this.cacheDom();
    },

    cacheDom(){
      this.inputForm = $(`#${this.key}-form`);
      this.inputItem = $(`#${this.key}-input-item`);
      this.addButton = $(`#${this.key}-add-button`);
      this.clearButton = $(`#${this.key}-clear-button`);
      this.incompleteUl = $(`#${this.key}-incomplete`);
      this.completeUl = $(`#${this.key}-complete`);
      this.delButtons = $('.del-button');
      this.doneChecks = $('.done-check');
      this.itemOrderSelectors = $('.item-order-selector');
      this.listItems = $(`#${this.key}-incomplete`).children('li');
    },

    liTemplate(task){
      let status = task.isCompleted;
      let li = $(`
        <li id="task_${task.order}">

          <input
            type="checkbox" id="check-${task.order}"
            name="check-${task.order}"
            class='done-check'>

          <select id="select-${task.order}" class="item-order-selector"></select>

          <label for="check-${task.order}">${task.description}</label>
          <button class="del-button">x</button>

        </li>
      `);

      li.children('input').attr('checked', status);

      return li;
    },

    appendAll(){
      this.list.tasks.forEach((task) => {
        let li = this.liTemplate(task);
        this.append(li, task.isCompleted);
      });
      this.cacheDom();

      this.appendOrderOptions();
      this.defaultOrderOptions();
      this.cacheDom();
    },

    append(li, isCompleted){
      let checkbox = li.children('input');

      if (isCompleted === false) {
        this.incompleteUl.append(li);
        checkbox.attr('checked', false);

      } else {
        li.children('select').remove();
        this.completeUl.append(li);
        checkbox.attr('checked', true)
      }
    },

    appendOrderOptions(){
      let options = [];
      this.listItems.each((index,e) => {
        let elem = $(e);
        let elemOrder = parseInt(this.getOrder(elem));
        options.push(`<option value="${index+1}">${index+1}</option>`);
      });

      options.forEach(option => {
        this.itemOrderSelectors.append(option);
      });
    },

    defaultOrderOptions(){
      this.listItems.each((index,e) => {
        let li = $(e);
        let listOrder = parseInt(this.getOrder(li));
        $(`#select-${listOrder} option[value=${index+1}]`).attr('selected','selected');
      });
    },

    getOrder(li){
      let order = li.attr('id').match(/_\w+/g).join().substring(1);
      //console.log(`GetOrder => elemOrder: ${order}`);
      return order;
    },

    reorder(){
      //reload lists
      this.incompleteUl.empty();
      this.completeUl.empty();
      this.appendAll();
      this.addDelEvents();
      this.addCheckEvents();
      this.addOrderSelectorEvents();
    },

    addInputEvent(){
      this.inputForm.on('submit', (e) => {
        e.preventDefault();
        let order = this.list.tasks.length;
        let description = this.inputItem.val();
        let isCompleted = false;

        let newTask = new task(order, description, isCompleted);

        this.list.add(newTask);
        let li = this.liTemplate(newTask);
        this.reorder();

        this.inputItem.val('');
        this.cacheDom();
      });
    },

    addDelEvents(){
      this.delButtons.click(e => {
        let order = this.getOrder($(e.currentTarget).closest('li'));
        this.list.remove(order);

        $(e.currentTarget).closest('li').remove();
        this.reorder();
      });
    },

    addCheckEvents(){
      this.doneChecks.on('change', (e) => {
        let li = $(e.currentTarget).closest('li');
        let order = this.getOrder(li);

        this.list.changeStatus(order);

        this.reorder();
      });
    },

    addOrderSelectorEvents(){
      this.itemOrderSelectors.on('change', (e) => {
        let selectedOrder = $(e.currentTarget).find('option:selected').text();
        let selectedLi = $(e.currentTarget).closest('li');
        let selectedLiOrder = this.getOrder(selectedLi);
        let selectedLiLabel = selectedLi.children('label').text();
        this.listItems.each((index,e) => {
          let li = $(e);
          let liOrder = this.getOrder(li);
          let targetLiOrder = li.children('select').find('option:selected').text();
          let targetLiLabel = li.children('label').text()
          if (selectedOrder === targetLiOrder && selectedLiLabel !== targetLiLabel) {
            //console.log(`selectOrder: ${selectedOrder}, targetLiOrder: ${targetLiOrder}`);
            //console.log(`labelText: ${targetLiLabel}`);
            this.list.switchTask(parseInt(selectedLiOrder), parseInt(liOrder));
            this.reorder();
          }
        });
      });
    },

    clearAll(){
      this.list.clearAll();
      this.incompleteUl.empty();
      this.completeUl.empty();
    }
  }

//////////////////////////////////////////////////////////////

  window.onload = () => {
  app.list = new taskList(app.key);

    const template = $(`
      <div id='${app.key}' class='one-third'>
          <section id='list-header'>
            <h1>${app.list.title}</h1>
            <form id='${app.key}-form'>
              <input type="text" id="${app.key}-input-item" placeholder="New">
              <input type="submit" value="Add" id="${app.key}-add-button" class='main-button'>
            </form>
          </section>

          <section id='${app.key}-list-main'>
            <ul id='${app.key}-incomplete' class='incomplete'></ul>
            <ul id='${app.key}-complete' class='complete'></ul>
          </section>

          <section id='${app.key}-list-footer'>
              <button id='${app.key}-clear-button' class='main-button'>Clear All</button>
          </section>
      </div>
    `);

    app.render(template);

    app.appendAll();

    app.addInputEvent();

    app.addDelEvents();

    app.addCheckEvents();

    app.addOrderSelectorEvents();

    app.clearButton.on('click', e => {
      app.clearAll();
    });
  }// end of window.onload
})();
