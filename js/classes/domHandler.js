class domHandler {
  constructor(key) {
    this._key = key;
  }

  cache() {
      this.inputForm = $(`#${this._key}-form`);
      this.inputItem = $(`#${this._key}-input-item`);
      this.addButton = $(`#${this._key}-add-button`);
      this.clearButton = $(`#${this._key}-clear-button`);
  }

  render(template) {
    $('main').append(template);
  }

  append() {
    //append an element to the dom
  }

  remove() {
    //remove an element from the dom
  }
}

export default domHandler;

// const userInputForm = document.getElementById('userInputForm');
// const inputItem = document.getElementById('inputItem')
// const listContainer = document.getElementById('listContainer');
// const clearButton = document.getElementById('clearButton');
// const addButton = document.getElementById('addButton');
//
// class domTaskList {
//   constructor (itemsArr) {
//     this.divNotDone = document.createElement('div');
//     this.divNotDone.id = 'div-not-done';
//
//     this.divDone = document.createElement('div');
//     this.divDone.id = 'div-done';
//
//     this.divNotDone.classList.add('row');
//     this.divDone.classList.add('row');
//
//     this.ulNotDone = document.createElement('ul');
//     this.ulNotDone.id = 'ul-not-done';
//     this.ulDone = document.createElement('ul');
//     this.ulDone.id = 'ul-done';
//   }
//
//   ulRender(){
//     //renders the ul that will contain the list
//     let h2NotDone = document.createElement('h2');
//     h2NotDone.textContent = 'Incomplete Tasks';
//     let h2Done = document.createElement('h2');
//     h2Done.textContent = 'Completed Tasks';
//
//     this.ulNotDone.classList.add('one-third');
//     this.ulDone.classList.add('one-third');
//
//     this.divNotDone.appendChild(h2NotDone);
//     this.divDone.appendChild(h2Done);
//     this.divNotDone.appendChild(this.ulNotDone);
//     this.divDone.appendChild(this.ulDone);
//
//     listContainer.appendChild(this.divNotDone);
//     listContainer.appendChild(this.divDone);
//   }
//
//   genId(itemObj, prefix){
//     //generates an id to reference an element in the dom
//     return prefix + itemObj.currentId;
//   }
//
//   liRender(itemObj){
//     //add a new li to the list
//     let currentId = itemObj.currentId;
//     let item = itemObj.item;
//     let checkState = itemObj.checkState;
//
//     let textDiv = document.createElement('div');
//     textDiv.textContent = item;
//     textDiv.classList.add('three-fourths', 'text-div');
//
//     let delButton = document.createElement('button');
//     delButton.id = this.genId(itemObj, 'delBtn');
//     delButton.classList.add('del-button','fa','fa-times', 'one-eighth');
//
//     let doneCheck = document.createElement('input');
//     doneCheck.setAttribute('type', 'checkbox');
//     doneCheck.id = this.genId(itemObj, 'check');
//     doneCheck.checked = checkState;
//     doneCheck.classList.add('done-check');
//
//     let liCodeBlock = document.createElement('li');
//     liCodeBlock.id = `li${currentId}`;
//     liCodeBlock.appendChild(doneCheck);
//     liCodeBlock.appendChild(textDiv);
//     liCodeBlock.appendChild(delButton);
//     liCodeBlock.classList.add('row');
//
//     if (checkState === false) {
//       this.ulNotDone.appendChild(liCodeBlock);
//     } else {
//       this.ulDone.appendChild(liCodeBlock);
//     }
//     this.addDelButtonListener(itemObj);
//     this.addCheckListener(itemObj);
//   }
//
//   addDelButtonListener (itemObj) {
//     let delButton = document.getElementById(this.genId(itemObj, 'delBtn'));
//     let currentId = itemObj.currentId;
//     delButton.addEventListener('click', () => {
//       // clear items with currentId from all lists
//       itemsList.clearItem(currentId);
//
//       //clear itemfrom the dom
//       this.liDelete(itemObj);
//     });
//   }
//
//   addCheckListener(itemObj){
//     //{'currentId' : currentId, 'checkId' : doneCheck.id, 'checkState' : checkState}
//     let check = document.getElementById(this.genId(itemObj, 'check'));
//     let currentId = itemObj.currentId;
//     let checkState = itemObj.checkState;
//     check.addEventListener('change', () => {
//       itemsList.list.forEach((obj, index) => {
//         if (obj.checkState === false && check.checked === true && obj.currentId === currentId) {
//           //going from false to true
//           console.log(`turn true`);
//
//           let oldItem = { ...obj };
//
//           itemsList.list[index].checkState = true;
//           itemsList.storeData();
//
//           //remove item from the divNotDone
//           this.liDelete(oldItem);
//
//           //add item to the divDone
//           this.liRender(itemsList.list[index]);
//         } else if (obj.checkState === true && check.checked === false && obj.currentId === currentId) {
//           //going from true to false
//           console.log(`turn false`);
//
//           let oldItem = { ...obj };
//
//           itemsList.list[index].checkState = false;
//           itemsList.storeData();
//
//           //remove item from the divDone
//           this.liDelete(oldItem);
//
//           //add item to the divNotDone
//           this.liRender(itemsList.list[index]);
//         }
//       });
//     });
//   }
//
//   liDelete(itemObj){
//     //deletes an li from the dom
//     let li = document.getElementById(this.genId(itemObj, 'li'));
//     console.log(itemObj.checkState);
//     let ul = document.getElementById((itemObj.checkState === false) ? 'ul-not-done' : 'ul-done');
//     ul.removeChild(li);
//   }
// }
//
// export default domTaskList;
