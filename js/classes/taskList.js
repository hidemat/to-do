class taskList {
  constructor(keyStr){
    this._key = keyStr
    this._title = this._key.charAt(0).toUpperCase() + this._key.slice(1).replace(/([A-Z])/g, ' $1')
    this._tasks = this.fetchData() ? this.fetchData('parse') : [];
  }

  get tasks (){
    return this._tasks;
  }

  get key (){
    return this._key;
  }

  get title(){
    return this._title;
  }

  add(task){
    // Add task to the List
    let order = task.order;
    this._tasks.splice(order, 0, task);

    this.storeData();
  }

  switchTask(selectedLiOrder, liOrder){
    // change the order of a task using the select tag
    this._tasks.forEach((task, index) => {
      if (selectedLiOrder === task.order) {
        task.order = liOrder;
        this.remove(index);
        this.add(task);
        this.reorder();
      }
    });
  }

  remove(order){
    // Remove task from list
    this._tasks.splice(order, 1);
    this.reorder();

    this.storeData();
  }

  changeStatus(order){
    let task = this._tasks[order];
    let status = task.isCompleted;

    task.isCompleted = status === false ? true : false ;
    this.storeData();
  }

  reorder(){
    // Match the current task indexes with the order of each task
    this._tasks.forEach((task, index) => {
      if (task.order !== index) {
        task.order = index;
      }
    });
    this.storeData();
  }

  fetchData (mode) {
    //fetches data from localStorage
    if (mode === 'parse') {
      return JSON.parse(localStorage.getItem(this._key));
    } else {
      return localStorage.getItem(this._key);
    }
  }

  storeData () {
    //Stores data in localStorage
    localStorage.setItem(this._key, JSON.stringify(this._tasks));
  }

  clearAll(){
    this._tasks = [];
    localStorage.removeItem(this._key);
  }
}

export default taskList;
