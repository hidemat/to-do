class task {
  constructor(order, description, isComplete){
    this.order = order;
    this.description = description;
    this.isCompleted = isComplete;
  }
}

export default task;
