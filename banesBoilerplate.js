(function() {
	class Task {
	    constructor(description, isComplete) {
	        this.description = description;
	        this.isCompleted = isComplete;
	    }
	}

	class TaskList {
		constructor() {
			this.tasks = [];
		}

		add(task) {
			// Add it
			// Save the data
		}

		remove(task) {
			// Remove it
			// Save the data
		}
	}

	window.onload = function() {
		var taskList = new TaskList();

		yourAddTaskEventHandlerGoesHere = e => {
			// Add your task to taskList
			// Render the new item
		};

		yourRemoveTaskEventHandlerGoesHere = e => {
			// Remove the task from taskList
			// Remove it from the DOM too
		};

		yourToggleStateEventHandlerGoesHere = e => {
			// Find the task in taskList and set the state
		};
	}
}());
